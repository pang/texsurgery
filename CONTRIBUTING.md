If you would like to contribute please open an issue and discuss your ideas, or contact the authors, or submit a PR, as you prefer.

## git

We use `git` for version control.

The server is https://framagit.org/pang/texsurgery, as you probably know already.
You must create a new user in order to contribute. Bear in mind that it can take a couple of days to get your account approved.

### Branches
You should use a branch and never edit directly on the master branch.
We usually don't create a branch for a simple update to the public info or documentation that cannot break anything, but use branches for anything else.

### Propose some changes

 - Create an issue, or comment on an existing issue as appropriate, stating your intentions.
 - `git pull`: pulls changes from the server
 - `git branch`: list local branches
 - `git checkout master`: switch to an existing branch
 - `git branch branch_name`: creates new branch `branch_name`, which forks from the current branch
 - `git checkout branch_name`: switch to the branch `branch_name` you just created
 - `git status`: lists files with changes
 - `git add files`: adds files to the next commit
 - `git status`: check again before you commit
 - `git commit`: creates a commit, which encapsulates changes to the files you added
 - `git push`: sends your changes to the server

### Review some other author's work:

 - `git branch -r`: list remote branches
 - `git fetch origin branch_name` downloads `branch_name`
 - `git checkout branch_name`: creates a new local branch to match the remote branch and updates your local files to match the remote branch.

### Merge request, etc...

 - If satisfied with your or your colleague's work at branch `branch_name`, create a new merge request at [framagit](https://framagit.org/pang/texsurgery/-/merge_requests/new).
 - If a project admin agrees that this work is ready to go, she can accept the merge request and close the corresponding issue.
