# TexSurgery

This tex package is a companion to the `texsurgery` `python` project:

[https://framagit.org/pang/texsurgery](https://framagit.org/pang/texsurgery)

This `LaTeX` library will make sure that

`pdflatex document.tex`

will work, with reasonable defaults, for a document that is intended to work with `texsurgery`, and also has other uses, always in tandem with the `texsurgery` `pypi` package.

However, remember that `texsurgery` is a `python` project whose main focus is on __evaluating code inside a `jupyter` kernel__, and this is _only_ achieved by installing the `python` package and calling the `texsurgery` command

`texsurgery -pdf document.tex`

## Usage

Once the `texsurgery` `CTAN` package is loaded at the preamble of yout TeX file, the compilation prints every `texsurgery`'s call to evaluate code included the TeX file as **verbatim** (either as a numerated marker with details or the literal code you want to evaluate), as well as a warning message with instructions at the beginning of your pdf document.

The `texsurgery` `CTAN` package admits passing the following options at the preamble, as `\usepackage[options]{texsurgery}`:

  - `noinstructions` -- removes the initial warning message both in the pdf and the compilation's log file.
  - `showcode` -- prints the literal code contained at every `texsurgery`'s call instead of a numerated marker.
  - `showoptions` -- shows the options included at every `texsurgery`'s call.

## License

The `texsurgery` `CTAN` package follows a [BSD license])(https://opensource.org/licenses/BSD-3-Clause), the same license as the `texsurgery` `python` project:

[https://framagit.org/pang/texsurgery/-/blob/master/LICENSE](https://framagit.org/pang/texsurgery/-/blob/master/LICENSE)

## texsurgery.pdf

The file `texsurgery.pdf` in the texsurgery CTAN LaTeX package was generated with the command

`pandoc README.md -o texsurgery.pdf`
