---
title: 'TexSurgery: Replace some commands and environments within a TeX document by evaluating code inside a jupyter kernel, and more'
tags:
  - LaTeX
  - jupyter
  - Python
  - julia
  - sagemath
authors:
  - name: Pablo Angulo
    orcid: 0000-0001-7511-561X
    equal-contrib: true
    affiliation: 1
  - name: Miguel Angel Marco Buzunariz
    orcid: 0000-0002-6750-8971
    equal-contrib: true # (This is how you can denote equal contributions between multiple authors)
    corresponding: true # (This is how to denote the corresponding author)
    affiliation: 2
  - name: Juan Viu Sos
    orcid: 0000-0001-9797-1337
    affiliation: 1
affiliations:
 - name: Universidad Politécnica de Madrid, Spain
   index: 1
 - name: Universidad de Zaragoza 
   index: 2
date: 3 January 2023
bibliography: paper.bib

---

# Summary

The usefuness of mixing LaTeX and code for generating reports has been recognized
since very early. In this sense, a major use case for `texsurgery` is simply to 
write jupyter notebooks in \LaTeX, possibly including different code written in
different programming languages.
There are also several use cases for parameterizing LaTeX documents, the generation 
of exercises and exams with variants being just one of them. Existing solutions for 
exam generation did not satisfy our requirements so we started projects:

 - `texsurgery`, based on building a solid foundation for the manipulation of 
   LaTeX documents.
 - `pyexams`, specifically focused on managing exams with variants.

TexSurgery allows to:

 - replace code written in any programming language with a `jupyter` kernel by its 
   output (our tests cover `python`, `julia` and `sagemath`)
 - offers powerful CSS-like selectors for find/insert/replace/shuffle
 - features a shell command that can be piped.

The filosophy of `texsurgery` is that of minimal intervention, in order not to
interfere with sophisticated \LaTeX packages.

# Statement of need

The convenience of creating a LaTeX document with embeded code and replacing
the code with the output of evaluating that code has been long recognized (see [Similar software])

Possible applications are, among others:

 - Literate Programming, where bare code is embedded into a document that
   explains not just how the code works, but also the motivations behind the
   design decisions. To us, this concept also includes the main use of
   `jupyter` notebooks, whichs is to deliver a code-heavy discourse, where the
   code is important, but the results and conclussions are even more important.
 - Automated scientific reports with a flexible and professional layout.
 - Automate the creation of certificates for attendance / participation /
   successful completion of a course etc
 - The original application that motivated us into creating `texsurgery` and
   `pyexams` was creating many variants of an exam, although the other use
   cases, and design common sense, indicated that they should be separate
   projects.

The utility of creating variants of an exam has been recognized by projects
such as [auto-multiple-choice](https://www.auto-multiple-choice.net/),
[R-exams](https://www.r-exams.org/), [moodle calculated question
type](https://docs.moodle.org/402/en/Calculated_question_type) and
[numbas](https://numbas.mathcentre.ac.uk/). However, none of them leverages the
features and stability of the widely succesful `jupyter` project, which was
designed from the beginning to be modular, and handle both errors and the
different types of output that the different interactive programming languages
offer. The jupyter project is very succesful, reaching the figure of 10 million
notebooks publicly available on github alone.

# Design principles

After trying and/or evaluating the available options in order to deliver
parametric exams during the covid 19 pandemics, the authors decided to start
the sister projects `texsurgery` and `pyexams`, based on the following
principles:

 1. Provide a experience as close as possible to the full LaTeX experience.
 2. However, don't do the actual job through the LaTeX engine, but instead 
    use `python` for all the text processing.
 3. Use the `jupyter` core libraries to outsource the evaluation of code in
    many programming languages, and the management of the possible errors.

Soon after starting working on the project, the following ideas were added
to the list

 4. Create a syntax for CSS-like selectors that allows for simple text edit
    operations, in the way that was pioneered by the
    [jquery](https://jquery.com/) javascript library.
 5. Offer the funcionality through a shell command that can be piped.

In the rest of this section, we explain the logic behind those decisions.

There are many software environments that allow the user to insert a limited
set of LaTeX commands. The target user for exam creation is a regular LaTeX 
user that can code, but is not an expert programmer. That user defines simple
macros and possibly uses sofisticated LaTeX packages such as 
[tikz](https://www.ctan.org/pkg/pgf) or 
[pstricks](https://www.ctan.org/pkg/pstricks-base). Including simple LaTeX
commands, as is allowed in `moodle` or in most markdown environments, does not
provide the full LaTeX experience. We wanted the user to be able to define her
own macros, and to use sofisticated libraries in the standard way.

The first design principle was, paradoxically, the main argument in favor of
the second. Some sofisticated software based in LaTeX, such as `sagetex` or
`automultiplechoice`, hack the \LaTeX `verbatim` environment in incompatible
ways, and do not work well together. The second argument in favor of using
`python` for the text processing was the quality, simplicity and flexibility of
python, and the richness of its libraries, such as `pyparsing` [@pyparsing],
which `texsurgery` uses. The third argument was that the sagetex author, Dan
Drake, documented all the tricks and hacks necessary to make `sagetex` work,
and we decided to avoid that approach [@sagetex]. The final argument was that
the authors' are very familiar with python, but not so much with \LaTeX.
Fortunately, the choice of `python` led naturally to choosing `jupyter`.

The reasons for calling a `jupyter` kernel through the `jupyter-client` 
libraries are obvious in retrospect. The project is very popular, with millions
of `jupyter` notebooks freely available using different kernels, which means it
offers a robust interface contemplating `html`, plain text and graphics output,
and a separate pipeline for errors. Furthermore, with no effort on our side,
`texsurgery` supports not only our initial target languages `python` and 
`sagemath`, but also many other popular choices such as `julia`, `octave`
or `R`, to name a few.

The itch for the CSS-like selectors initially came from the requirements for
exam creation, which requires to shuffle items within a list, collect all
environments of a certain kind, add `usepackage` statements, and several other
similar tasks. Our experience with the `html`+`CSS`+ `javascript` pack made us 
realize that a similar construction could make the job easy and enjoyable, and 
allow for many other use cases. Please read the package documentation for 
illustrations of the possibilities.

Finally, using such a system through the command line completes the funcionality
of `texsurgery` with that of any other shell command, thus allowing the creation
of very simple shell scripts that perform sofisticated jobs that can be scheduled
with `cron`, called from any programming language without interfacing to python,
or incorporated to file managers as custom actions, to name a few of the possible
uses.

`texsurgery` also allows for a final possibility: defining \LaTeX snippets with 
`\eval` commands, thus becoming \LaTeX commands with arguments, only written in
the users' favourite language.

 - TODO: jviu promissed an example of such snippets-as-functions

# Similar software

## Literate Programming

 - [CWEB](http://www.literateprogramming.com/) [@CWEB] and [Haskell native tools](https://wiki.haskell.org/Literate_programming) for Literate Programming.

## sagetex

This project brought the functionality of Literate Haskell to `sagemath` users. 
Its functionality, design choices and user interface were a major source of 
inspiration for `texsurgery`, whether we dediced to follow `sagetex` or not.

Our major point of departure, beyond the extra funcionality that has already
been accounted for, is that `sagetex` relies on the \LaTeX engine for the actual
work of replacing code by its output (although not for running the code), and
the dependence on the `verbatim` environment, which is a kind of hack with long
reaching ramifications.

TODO: comment on sagetex and pythontex design, and you could achieve the same
capabilities with texsurgery...

## pythontex

A natural continuation of `sagetex` that uses `python`, but also allows code in
any programming language, requiring a template to be filled.
The 

## R-markdown and sweave

 - [R sweave](https://en.wikipedia.org/wiki/Sweave) and its successor [knitr](https://yihui.org/knitr) [@KNITR], mainly for automated report generation

## fp

This \LaTeX package allows for random number generation, and the functionality of
a scientific calculator in \LaTeX. Although simple, it does not stretch \LaTeX
beyond its zone of comfort in the way that `sagetex` does, and it is thus 
compatible with auto-multiple-choice.

However, we consider not just `fp`, but the whole approach of doing computation through a \LaTeX package insuficient, since tasks that are easily done in popular programming languages with jupyter kernels become either cumbersome or impossible, and it would require the user to learn a new tool that

## jupyter and papermill

`jupyter` allows the graceful combination of code, output, and comments, and
can generate nicely formatted `pdf` files, based on \LaTeX.
The software package
[papermill](https://papermill.readthedocs.io)
simplifies automation in order to run variants of a notebook, changing the values of a few parameters.
Thus, using this, or other custom tools, it is relatively simple to generate pdf files coming from `jupyter` notebooks, but this approach greatly sacrifies the flexibility of \LaTeX for creating any kind of file.

# Mathematics

Single dollars ($) are required for inline mathematics e.g. $f(x) = e^{\pi/x}$

Double dollars make self-standing equations:

$$\Theta(x) = \left\{\begin{array}{l}
0\textrm{ if } x < 0\cr
1\textrm{ else}
\end{array}\right.$$

You can also use plain \LaTeX for equations
\begin{equation}\label{eq:fourier}
\hat f(\omega) = \int_{-\infty}^{\infty} f(x) e^{i\omega x} dx
\end{equation}
and refer to \autoref{eq:fourier} from text.

# Citations

Citations to entries in paper.bib should be in
[rMarkdown](http://rmarkdown.rstudio.com/authoring_bibliographies_and_citations.html)
format.

If you want to cite a software repository URL (e.g. something on GitHub without a preferred
citation) then you can do it with the example BibTeX entry below for @fidgit.

For a quick reference, the following citation commands can be used:
- `@author:2001`  ->  "Author et al. (2001)"
- `[@author:2001]` -> "(Author et al., 2001)"
- `[@author1:2001; @author2:2001]` -> "(Author1 et al., 2001; Author2 et al., 2002)"

# Figures

Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

Figure sizes can be customized by adding an optional second parameter:
![Caption for example figure.](figure.png){ width=20% }

# Thanks

To all our colleagues that gave feedback to the early versions, specially Fabricio from ETSIN.UPM and Carlos from ETSIAAB.UPM

# References
