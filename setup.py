import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="texsurgery",
    version="0.6.3",
    author="Pablo Angulo, Juan Viu Sos, Miguel Angel Marco Buzunariz",
    author_email="pablo.angulo@upm.es",
    description="Replace some commands and environments within a TeX document" \
                " by evaluating code inside a jupyter kernel",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/pang/texsurgery",
    # https://stackoverflow.com/questions/61624018/include-extra-file-in-a-python-package-using-setuptools
    include_package_data=True,
    packages=setuptools.find_packages() + ['texsurgery/tex/texsurgery'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
      'jupyter_client',
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': ['texsurgery=texsurgery.command_line:main'],
    },
)
