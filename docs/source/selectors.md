## Selectors

### `find` and `findall`

`texsurgery` can also gather information using css-style selectors:

.. doctest::

    >>> from texsurgery.texsurgery import TexSurgery
    >>> tex = open('../tests/test_find.tex').read()
    >>> # An environment, which can be question or questionmultx,
    >>> # which contains an environment runsilent, and captures its content
    >>> TexSurgery(tex).findall('question,questionmultx runsilent')
    [('questionmultx', [('runsilent', '\na = randint(1,10)\n')]), ('questionmultx', [('runsilent', '\na = randint(2,10)\n')]), ('question', [('runsilent', '\na = randint(2,10)\nf = sin(a*x)\nfd = f.derivative(x)\n')])]
    >>> # An environment, which can be question or questionmultx,
    >>> # which contains an environment choices
    >>> # which contains a command \correctchoice, and captures its argument
    >>> TexSurgery(tex).findall('question,questionmultx choices \correctchoice{choice}')
    [('question', [('choices', [('\\correctchoice', {'choice': '$\\sage{fd}$'})])])]
    >>> # An environment questionmultx which contains a command
    >>> # \AMCnumericChoices with two arbitrary arguments
    >>> TexSurgery(tex).findall('questionmultx \AMCnumericChoices[_nargs=2]')
    [('questionmultx', [('\\AMCnumericChoices', {'arg0': '\\eval{8+a}', 'arg1': 'digits=2,sign=false,scoreexact=3'})]), ('questionmultx', [('\\AMCnumericChoices', {'arg0': '\\eval{8*a}', 'arg1': 'digits=2,sign=false,scoreexact=3'})])]
    >>> # Environment questionmultx, with first argument exactly equal to basic-multiplication
    >>> TexSurgery(tex).find(r'questionmultx{@basic-multiplication}')
    ('questionmultx', {'@basic-multiplication': 'basic-multiplication'}, '\n\\begin{runsilent}\na = randint(2,10)\n\\end{runsilent}\nWhat is $8*\\eval{a}$?\n\\AMCnumericChoices{\\eval{8*a}}{digits=2,sign=false,scoreexact=3}\n')
    >>> # Command \copygroup, with any first argument, and second argument
    >>> # exactly equal to BigGroupe
    >>> TexSurgery(tex).findall(r'\copygroup{category}{@BigGroupe}')
    [('\\copygroup', {'category': 'cat1', '@BigGroupe': 'BigGroupe'}), ('\\copygroup', {'category': 'cat2', '@BigGroupe': 'BigGroupe'})]
    >>> # Command \subsection, with any title as first argument,
    >>> # with \label{seed}
    >>> TexSurgery(tex).find('\\subsection[label="student id"]{title}')
    ('\\subsection', {'title': 'Student identification'})
    >>> # Command \subsection, with any title as first argument,
    >>> # with \label{seed} and in this subsection (which is ended by a \section)
    >>> # there is a run environment
    >>> TexSurgery(tex).find('\\subsection{title}#seed:next run')
    ('\\subsection', ('run', "\nprint('The random seed is ', seed)\n"))

### `insertAfter` and `replace`

`texsurgery` can perform search-and-replace, and search-and-insert-after using the css-style selectors:

.. doctest::

    >>> from texsurgery.texsurgery import TexSurgery
    >>> tex = r'''\begin{choices}
    ... \wrongchoice{$\sage{fd + a}$}
    ... \correctchoice{$\sage{fd}$}
    ... \wrongchoice{$\sage{fd*a}$}
    ... \end{choices}
    ... '''
    >>> ts = TexSurgery(tex)
    >>> ts = ts.replace(r'\correctchoice{choice}', r'\correctchoice{$\sage{f.derivative(x)}$}')
    >>> ts.src
    '\\begin{choices}\n\\wrongchoice{$\\sage{fd + a}$}\n\\correctchoice{$\\sage{f.derivative(x)}$}\n\\wrongchoice{$\\sage{fd*a}$}\n\\end{choices}\n'

### `shuffle`

`texsurgery` can also shuffle some TexElements nested within a parent using the css-style selectors:

.. doctest::

    >>> from texsurgery.texsurgery import TexSurgery
    >>> tex = r'''\begin{choices}
    ... \wrongchoice{$\sage{fd + a}$}
    ... \correctchoice{$\sage{fd}$}
    ... \wrongchoice{$\sage{fd*a}$}
    ... \end{choices}
    ... '''
    >>> ts = TexSurgery(tex)
    >>> ts = ts.shuffle('choices', r'\correctchoice{choice},\wrongchoice{choice}', randomseed=1)
    >>> ts.src
    '\\begin{choices}\n\\correctchoice{$\\sage{fd}$}\n\\wrongchoice{$\\sage{fd*a}$}\n\\wrongchoice{$\\sage{fd + a}$}\n\\end{choices}\n'

.
