texsurgery package
==================

texsurgery.texsurgery module
----------------------------

.. automodule:: texsurgery.texsurgery
   :members:
   :undoc-members:
   :show-inheritance:

texsurgery.simplekernel module
------------------------------

.. automodule:: texsurgery.simplekernel
   :members:
   :undoc-members:
   :show-inheritance:
