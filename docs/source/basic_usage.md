## Basic usage

The most simple use case is to insert a jupyter kernel output in a latex document. In order to do so, the document must include a line like

```
\usepackage[python3]{texsurgery}
```

to tell `texsurgery` which kernel to use. Then texsurgery can perform the following substitutions:

### Environments

#### run

Code blocks like

```
\begin{run}
code
\end{run}
```

will pass `code` to the kernel, and then be replaced by the output (both the input and the output may consist on several lines).

#### runsilent

Works like `run` but no output is shown.

#### eval

allows to make a substitution of a single line input in the middle of a line. For example

```
The result of 2+2 is \eval{2+2}.
```

will be replaced to

```
The result of 2+2 is 4.
```

##### Formatting

Optional parameters `type` and `format` can be passed to `eval` in order to format the result.

`type` can be any python type, plus `string` and `tex`. The output is converted to the corresponding type (`string` works like `str`, if the outout is a string between quotes, the quotes are eliminated; `tex` comverts a latex expression with escaed charcters to its unescaped version).

`format` can be any python format string, or `upper` or `lower`.

If the type is not provided, it will be infered from the format if possible.

So for example, the following code

```
\eval[type=float,format=.2f]{3*5}
\eval[type=float,format=8.2f]{3*5}
\eval[format=upper]{'a'+'b'}
\eval[type=string,format=upper]{'a'+'b'}
\eval[type=tex]{'\\frac{3}{5}'}
```

will result in

```
15.00
   15.00
'AB'
AB
\frac{3}{5}
```


#### evaltex

Like `eval`, but escaping special characters from raw strings.

#### evalstr

Like `evaltex`, but the special characters are also translated to the corresponding LaTeX code.


#### sage

A command for evaluating a statement in the `sagemath` kernel. The output goes through the `sagemath` function `latex()`.
E.g.

```latex
The function $x\rightarrow \sage{sin(pi*x)}$...
```
is transformed into

```latex
The function $x\rightarrow \sin\left(\pi x\right)$...
```

<!---
gitlab ci does not have the sagemath kernel, so we skip this doctest
.. testcode::
    :hide:

    from texsurgery.texsurgery import TexSurgery
    tex_source = '\\usepackage[sagemath]{texsurgery}The function $x\\rightarrow \\sage{sin(pi*x)}$...'
    ts = TexSurgery(tex_source, verbose=False)
    print(ts.code_surgery().src)

.. testoutput::

    The function $x\rightarrow \sin\left(\pi x\right)$...
--->

#### sinput

Open a file and run it in the kernel: `\sinput{file.py}`

#### sif

```latex
\sif{code}{tex1}{tex2}
```

Run statement `code`:
  - if it evals to ~true~, then `tex1` is inserted into the tex file, and `tex2` is discarded.
  - if it evals to ~false~, the opposite happens.

For instance

```latex
\begin{runsilent} a=8 \end{runsilent}
Number \eval{a} is \sif{a%3==0}{}{not} a multiple of 3
```

.. testcode::
    :hide:

    from texsurgery.texsurgery import TexSurgery
    tex = r'\usepackage[python3]{texsurgery}' \
    r'\begin{runsilent} a=8 \end{runsilent}' \
    r'Number \eval{a} is \sif{a%3==0}{}{not} a multiple of 3'
    ts = TexSurgery(tex, verbose=False)
    print(ts.code_surgery().src)

.. testoutput::

    \usepackage[python3,noinstructions]{texsurgery}Number 8 is not a multiple of 3


#### srepl

Use a `srepl` environment for a block of code that reflects what happens in an interactive session

For instance

```latex
\begin{srepl}[kernel=python3]
1 + 1
'1' + '1'
\end{srepl}
```


.. testcode::
    :hide:

    tex = r'''\usepackage[python3]{texsurgery}\begin{srepl}[kernel=python3]
    1 + 1
    '1' + '1'
    \end{srepl}'''
    from texsurgery.texsurgery import TexSurgery
    ts = TexSurgery(tex, verbose=False)
    print(ts.code_surgery().src)

.. testoutput::

    \usepackage[python3,noinstructions]{texsurgery}\begin{verbatim}
    >>> 1 + 1
    2
    >>> '1' + '1'
    '11'
    \end{verbatim}

.

