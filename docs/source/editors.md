## Usage in editors

TexSurgery can be used from the usual latex editors. In order to do so,
you should configure your editor.

Here are some basic instructions for some of them:

### GNOME LaTeX

Go to "Build" -> "Manage build tools".
On the "personal build tools" click the "+" icon. Fill the following fields:

- Label : `TexSurgery`
- Description : `TeXSurgery`
- Extensions: `.tex`
- Commands: `texsurgery $filename -pdf --pdflatex-options  -synctex=1`
- Post-Processing: `all-output`
- Open file: `$shortname.pdf`

### Kile

Add a new compile option going to
Settings -> Configure Kile -> Build ->  New...
and choose a name (TeXSurgery should be the natural choice), then select PDFLaTeX as a base template.

Then edit it writing the path to your texsurgery executable (tipically, `/usr/local/bin/texsurgery`
or `/home/YOUR_USERNAME/.local/bin/texsurgery` depending on wheather you installed it system-wide or user-only) on the
"Command" field. On the "options" field, put `%source -pdf --pdflatex-options -synctex=1 -interaction=nonstopmode`.

You can also go to the "Menu" tab to choose in which Menu will the option to compile with TeXSurgery appear.

### Texmaker

Go to "Options" -> "Configure Texmaker" and select the "Quick build" tab. Select the "user" option
and change the text field to `texsurgery %.tex -pdf --pdflatex-options -synctex=1 -interaction=nonstopmode`.
Click the accept button.

### TeXworks

Go to
Edit -> Preferences -> Build, and in the "Processing tools" section, click on the "+" icon to add a new tool.

Then fill the name you want for the tool, put `texsurgery` in the command field, and then add the following
options (in this order) : `$fullname`, `-pdf`, `--pdflatex-options`, `$synctexoption`


