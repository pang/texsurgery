.. texsurgery documentation master file, created by
   sphinx-quickstart on Mon Jun 14 19:19:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../../README.md


Contents
--------

.. toctree::
   :maxdepth: 3

   install
   testing
   editors
   basic_usage
   command_line
   kernels
   selectors
   texsurgery


