## Command line usage

You can call it from the command line as follows:

```
texsurgery input_file.tex -o output_file.tex
```

will perform the code substitutions in `input_file.tex` and write the result in `output_file.tex`

The possible parameters are:

- `-h, --help` shows the help message and exits,
- `input_file` the file to read from. If none given, the standard input will be used.
- `--output_file output_file, -o output_file` writes the result in output file. If it is not provided, the result is directed to the standard output
- `-tex` just perform the code substitution in the latex content (this is the default)
- `-pdf` create a pdf file from the resulting modified tex. If the `-o` option is not provided, texsurgery will try to guess the name of the resulting file. This option deppends on `pdflatex`being installed in the system.
- `--pdflatex-options` options to be passed to pdflatex (it requires the `-pdf` option).


### piping commands

Since it can use standard input/output, it is pipe friendly. So, for example:

```
texsurgery input_file.tex | pandoc --from latex -o output_file.html
```
will perform the code substitutions and convert the result to a html (provided you have `pandoc` installed).

Likewise, if you have a markdown file `input.md` with the lines

```
\usepackage[python3,noinstructions]{texsurgery}

The result of multiplying 3 and 5 is \eval{3*5}
```

And run

```
pandoc input.md --from markdown --to latex -s | texsurgery
```

you will get the corresponding latex file with the `\eval{3*5}` text substituted by the corresponding result `15`.

In fact, since texsurgery does not require fully correct `.tex` code, you can directly run texsurgery in the markdown file!

### Selectors and other command line options

Other texsurgery utils are available from the command line:

 - `texsurgery -data sample.json input.tex`

Replaces `\command1` by `value1`, ..., `\commandN` by `valueN` in `input.tex`, if `sample.json` is
```
{'command1':value1, ... , 'commandN':valueN}

```

 - `texsurgery -find selector input.tex`

Finds the first occurrence of `selector` in file `input.tex`

 - `texsurgery -replace selector tex_replacement input.tex`

Replaces all occurrences of `selector` by `tex_replacement` in file `input.tex`.

 - `texsurgery -shuffle parent_selector inner_selector`


Shuffles all occurrences of `inner_selector` within any `parent_selector`.

