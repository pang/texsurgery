## Testing

If you installed from source, the following command will perform some common tests, and specific tests for some of the kernels that are installed:

    python3 -m unittest tests

