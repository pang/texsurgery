#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import logging

from texsurgery.texsurgery import TexSurgery

class TestJuliaSurgery(unittest.TestCase):
    """ Tests TexSurgery.code_surgery for the julia kernel"""

    def test_simple_addition(self):
        """ Tests a simple addition"""
        tex_source = r'\usepackage[julia]{texsurgery}2+2=\eval{2+2}'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}2+2=4'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

    def test_sif(self):
        r""" Tests \sif{}{}{}"""
        tex_source = r'\usepackage[julia]{texsurgery}3 is an' \
                     r' \sif{3 % 2==1}{odd}{even} number'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}3 is an odd number'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

        tex_source = r'\usepackage[julia]{texsurgery}4 is an' \
                     r' \sif{4 % 2==1}{odd}{even} number'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}4 is an even number'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

    def test_string_interpolation(self):
        r""" Tests $ replacing of vars in julia:
        https://docs.julialang.org/en/v1/manual/strings/#string-interpolation
        """
        tex_source = r'\usepackage[julia]{texsurgery}' \
                     r'\evalstr{"1 + 2 = $(1 + 2)"}'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}1 + 2 = 3'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

        tex_source = r'\usepackage[julia]{texsurgery}' \
                     r'\begin{runsilent}πhalf = π/2\end{runsilent}' \
                     r'\evalstr{"sin($πhalf)=1"}'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}sin(1.5707963267948966)=1'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

        tex_source = r'\usepackage[julia]{texsurgery}' \
                     r'\begin{runsilent}using Printf;πhalf = π/2\end{runsilent}' \
                     r'\evalstr{@printf("%.2f", πhalf)}'
        tex_out = r'\usepackage[julia,noinstructions]{texsurgery}1.57'
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel
