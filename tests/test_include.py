#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import sys


class TestInclude(unittest.TestCase):
    """ Tests command_line.py with multiple source files"""

    def test_bundle(self):
        """ Tests a simple include with -bundle option"""
        tex_out = r'''\usepackage[python3,noinstructions]{texsurgery}
\begin{document}
File \texttt{child.tex} is included:
\clearpage
Here variable \texttt{a} is defined


\clearpage

In that file, variable \texttt{a} = 5 is defined
\end{document}
'''
        sys.argv = ['texsurgery',
                    '-bundle',
                    '-o',
                    'tests/test_include_out_tmpfile.tex',
                    'tests/root.tex']
        # import after modifying sys.argv
        from texsurgery.command_line import main
        main()
        with open('tests/test_include_out_tmpfile.tex', 'r') as f:
            tex_in_outfile = f.read()
        self.assertEqual(tex_in_outfile, tex_out)

    def test_follow_error(self):
        """ Tests a simple include with -follow option that raises an Error

        A variable is defined in child.tex, and then recalled in root.tex 
        """
        tex_out = r'''\usepackage[python3,noinstructions]{texsurgery}
\begin{document}
File \texttt{child.tex} is included:
\clearpage
Here variable \texttt{a} is defined


\clearpage

In that file, variable \texttt{a} = 5 is defined
\end{document}
'''
        sys.argv = ['texsurgery',
                    '-follow',
                    '-o',
                    'tests/test_include_out_tmpfile.tex',
                    'tests/root.tex']
        # import after modifying sys.argv
        from texsurgery.command_line import main
        self.assertRaises(AttributeError, main)

    def test_follow_bundle_default(self):
        """Test -follow, -bundle and no options onthe same file"""
        tex_out_default = r'''\usepackage[python3,noinstructions]{texsurgery}
\begin{document}
First we define a=3

Now \texttt{a} = 3.
Then file \texttt{child.tex} is included, which defines a=5:
\include{tests/child}
Finally, \texttt{a} = 3.
\end{document}
'''
        sys.argv = ['texsurgery',
                    '-o',
                    'tests/test_include_out_tmpfile.tex',
                    'tests/root2.tex']
        # import after modifying sys.argv
        from texsurgery.command_line import main
        main()
        with open('tests/test_include_out_tmpfile.tex', 'r') as f:
            tex_in_outfile = f.read()
        self.assertEqual(tex_in_outfile, tex_out_default)

        tex_out_follow = r'''\usepackage[python3,noinstructions]{texsurgery}
\begin{document}
First we define a=3

Now \texttt{a} = 3.
Then file \texttt{child.tex} is included, which defines a=5:
\include{tests/child.modified_texsurgery_0}
Finally, \texttt{a} = 3.
\end{document}
'''
        sys.argv = ['texsurgery',
                    '-follow',
                    '-o',
                    'tests/test_include_out_tmpfile.tex',
                    'tests/root2.tex']
        # import after modifying sys.argv
        from texsurgery.command_line import main
        main()
        with open('tests/test_include_out_tmpfile.tex', 'r') as f:
            tex_in_outfile = f.read()
        self.assertEqual(tex_in_outfile, tex_out_follow)
        tex_aux = 'Here variable \\texttt{a} is defined\n\n'
        with open('tests/child.modified_texsurgery_0.tex', 'r') as faux:
            tex_in_auxfile = faux.read()
        self.assertEqual(tex_in_auxfile, tex_aux) 

        tex_out_bundle = r'''\usepackage[python3,noinstructions]{texsurgery}
\begin{document}
First we define a=3

Now \texttt{a} = 3.
Then file \texttt{child.tex} is included, which defines a=5:
\clearpage
Here variable \texttt{a} is defined


\clearpage

Finally, \texttt{a} = 5.
\end{document}
'''
        sys.argv = ['texsurgery',
                    '-bundle',
                    '-o',
                    'tests/test_include_out_tmpfile.tex',
                    'tests/root2.tex']
        # import after modifying sys.argv
        from texsurgery.command_line import main
        main()
        with open('tests/test_include_out_tmpfile.tex', 'r') as f:
            tex_in_outfile = f.read()
        self.assertEqual(tex_in_outfile, tex_out_bundle)


