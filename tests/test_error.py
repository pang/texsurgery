#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import logging

from texsurgery.texsurgery import TexSurgery


class TestPython3Surgery(unittest.TestCase):

    def test_error(self):
        """ Tests a Division by zero"""
        tex_source = r'\usepackage[python3]{texsurgery}1/0=\eval{1/0} '
        tex_out = '\\usepackage[python3,noinstructions]{texsurgery}1/0=\n\\begin{verbatim}### Code error ###\n\x1b[0;31m---------------------------------------------------------------------------\x1b[0m\n\x1b[0;31mZeroDivisionError\x1b[0m                         Traceback (most recent call last)\n\x1b[0;32m<ipython-input-1-9e1622b385b6>\x1b[0m in \x1b[0;36m<module>\x1b[0;34m\x1b[0m\n\x1b[0;32m----> 1\x1b[0;31m \x1b[0;36m1\x1b[0m\x1b[0;34m/\x1b[0m\x1b[0;36m0\x1b[0m\x1b[0;34m\x1b[0m\x1b[0;34m\x1b[0m\x1b[0m\n\x1b[0m\n\x1b[0;31mZeroDivisionError\x1b[0m: division by zero\\begin{verbatim} '
        ts = TexSurgery(tex_source).code_surgery()
        self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel


    def test_error_do_not_allow(self):
        """ Tests a Division by zero"""
        tex_source = r'\usepackage[python3]{texsurgery}1/0=\eval{1/0} '
        with self.assertRaises(SystemExit) as cm:
            ts = TexSurgery(tex_source, allow_errors_when_output=False).code_surgery()

        self.assertEqual(cm.exception.code, 1)
