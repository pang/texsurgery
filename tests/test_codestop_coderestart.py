#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import logging

from texsurgery.texsurgery import TexSurgery


class TestPython3Surgery(unittest.TestCase):
    """ Tests TexSurgery.code_surgery for the python3 kernel"""

    def test_codestop_simple(self):
        """ Define a var, codestop, check that eval is left as-is"""
        tex_source = r'''\usepackage[python3]{texsurgery}
\begin{runsilent}a=1\end{runsilent}
\codestop
a=\eval{a}
'''    
        tex_out = r'''\usepackage[python3,noinstructions]{texsurgery}


a=\eval{a}
'''    
        ts = TexSurgery(tex_source).code_surgery()    
        self.assertEqual(ts.src, tex_out)    
        del ts  # shutdown kernel

    def test_codestop_variants(self):
        """ Same as test_codestop_simple, but for the variants of eval"""
        for runvariant in ('run', 'runsilent'):
            for evalvariant in ('eval', 'evalstr', 'evaltex', 'sage'):
                tex_source = r'''\usepackage[python3]{{texsurgery}}
\begin{{{runvariant}}}a=1;\end{{{runvariant}}}
\codestop
a=\{evalvariant}{{a}}'''.format(runvariant=runvariant, evalvariant=evalvariant)
                tex_out = r'''\usepackage[python3,noinstructions]{{texsurgery}}


a=\{evalvariant}{{a}}'''.format(evalvariant=evalvariant)
                ts = TexSurgery(tex_source).code_surgery()
                print('#'*20)
                print(runvariant, evalvariant)
                print(tex_source,tex_out)
                self.assertEqual(ts.src, tex_out)
        del ts  # shutdown kernel

    def test_codestop_coderesume(self):
        """ Define a var, codestop, redefine the var, coderesume, print"""
        tex_source = r'''\usepackage[python3]{texsurgery}
\begin{runsilent}a=1\end{runsilent}
\codestop
\begin{runsilent}a=2\end{runsilent}
\coderesume
a=\eval{a}
'''    
        tex_out = r'''\usepackage[python3,noinstructions]{texsurgery}




a=1
'''    
        ts = TexSurgery(tex_source).code_surgery()    
        self.assertEqual(ts.src, tex_out)    
        del ts  # shutdown kernel    

    def test_coderestart(self):
        """ Redefine sum, coderestart, print"""
        tex_source = r'''\usepackage[python3]{texsurgery}
\begin{runsilent}sum = lambda xs: max(xs)\end{runsilent}
1+1=\eval{sum([1,1])}
\coderestart
1+1=\eval{sum([1,1])}
'''
        tex_out = r'''\usepackage[python3,noinstructions]{texsurgery}

1+1=1

1+1=2
'''
        ts = TexSurgery(tex_source).code_surgery()    
        self.assertEqual(ts.src, tex_out)    
        del ts  # shutdown kernel    


