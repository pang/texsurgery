import importlib
import os.path

from jupyter_client.kernelspec import KernelSpecManager

from .test_simplekernel import *
from .test_data_surgery import *
from .test_imports import *
from .test_find import *
from .test_insert_replace import *
from .test_shuffle import *
from .test_command_line import *
from .test_format import *
from .test_codestop_coderestart import *

def import_kernel_tests(module_name):
    mdl = importlib.import_module(module_name, package='tests')
    # is there an __all__?  if so respect it
    if "__all__" in mdl.__dict__:
        names = mdl.__dict__["__all__"]
    else:
        # otherwise we import all names that don't begin with _
        names = [x for x in mdl.__dict__ if not x.startswith("_")]
    globals().update({k: getattr(mdl, k) for k in names})


def test_all():
    ksm = KernelSpecManager()

    has_sagemathxx = False
    has_sagemath = False
    has_juliaxx = False
    has_julia = False
    for k in ksm.get_all_specs():
        if k == 'sagemath':
            has_sagemath = True
        elif k.startswith('sagemath'):
            has_sagemathxx = True
        elif k == 'julia':
            has_julia = True
        elif k.startswith('julia'):
            has_juliaxx = True
        try:
            print('Start tests for kernel', k)
            import_kernel_tests('.test_%s' % k)
            if os.path.exists('tests/test_%s_integration.py' % k):
                import_kernel_tests('.test_%s_integration' % k)
            print('Done')
        except ModuleNotFoundError:
            print('No specific tests for installed kernel ', k)
        print('-'*30)

    if 'python3' not in ksm.get_all_specs():
        print('python3 kernel appears not to be installed, could not be tested')
    if not has_sagemath and not has_sagemathxx:
        print('sagemath kernel appears not to be installed, could not be tested')
    elif has_sagemathxx and not has_sagemath:
        print('Start tests for kernel sagemath')
        import_kernel_tests('.test_sagemath')
    if has_sagemathxx or has_sagemath:
        import_kernel_tests('.test_multikernel')
    if not has_julia and not has_juliaxx:
        print('julia kernel appears not to be installed, could not be tested')
    elif has_juliaxx and not has_julia:
        print('Start tests for kernel julia')
        import_kernel_tests('.test_julia')


test_all()
